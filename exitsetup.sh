#!/bin/bash

# Check if the script is run with root privileges
if [ "$(id -u)" -ne 0 ]; then
  echo "Please run the script as root or using sudo."
  exit 1
fi

# Prompt user for relay nickname and contact information
read -p "Enter your relay nickname: " relay_nickname
read -p "Enter your contact information: " contact_info

# Install apt-transport-tor and gnupg
apt-get update
apt-get install -y apt-transport-tor gnupg

# Create a new file /etc/apt/sources.list.d/tor.list
tor_list_file="/etc/apt/sources.list.d/tor.list"
echo "deb [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] tor://apow7mjfryruh65chtdydfmqfpj5btws7nbocgtaovhvezgccyjazpqd.onion/torproject.org $(lsb_release -cs) main" > "$tor_list_file"

# Download Tor archive key and import it to the keyring
wget -qO- https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --dearmor | tee /usr/share/keyrings/tor-archive-keyring.gpg >/dev/null

# Update apt
apt-get update

# Install Tor and deb.torproject.org-keyring
apt-get install -y tor deb.torproject.org-keyring

# Download torrc file
torrc_file="/etc/tor/torrc"
wget -O "$torrc_file" https://codeberg.org/phin/torrelayexitsetup/raw/branch/main/torrc

# Replace placeholders in the torrc file
sed -i "s/<myexitnick>/$relay_nickname/g" "$torrc_file"
sed -i "s/<mycontact>/$contact_info/g" "$torrc_file"

# Inform user about completion
echo "Configuration completed. Tor is installed, and the relay is configured."
